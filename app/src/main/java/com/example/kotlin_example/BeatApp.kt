package com.example.kotlin_example

import android.app.Application
import android.content.Context
import com.example.kotlin_example.base.presentation.components.AppComponent
import com.example.kotlin_example.base.presentation.components.DaggerAppComponent
import com.example.kotlin_example.base.presentation.modules.AppModule
import com.example.kotlin_example.base.presentation.modules.NetModule
import java.lang.IllegalArgumentException

class BeatApp : Application() {

    val applicationComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(
                AppModule(
                    this
                )
            )
            .netModule(
                NetModule(
                    "https://private-d599c5-clothes.apiary-mock.com/"
                )
            )
            .build()
    }

}

fun Context.getAppComponent(): AppComponent {
    if (this.applicationContext !is BeatApp) {
        throw IllegalArgumentException("Context provided is out of app context")
    }

    val app = this.applicationContext as BeatApp
    return app.applicationComponent
}
