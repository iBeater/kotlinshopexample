package com.example.kotlin_example.splash

import android.content.Intent
import android.os.Bundle
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.activities.BaseActivity
import com.example.kotlin_example.home.presentation.activities.HomeActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {

    override fun initView(savedInstanceState: Bundle?) {
        splashAnimation()
    }

    override fun getLayout(): Int = R.layout.activity_splash

    private fun splashAnimation() {
        splash_image.alpha = 0f
        splash_image.animate().setDuration(2500).alpha(1f).withEndAction {
            val i = Intent(this, HomeActivity::class.java)
            startActivity(i)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }
    }
}