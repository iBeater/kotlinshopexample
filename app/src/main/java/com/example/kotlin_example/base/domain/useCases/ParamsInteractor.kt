package com.example.kotlin_example.base.domain.useCases

import io.reactivex.observers.DisposableObserver

interface ParamsInteractor<Result, Params>{
    fun execute(observer: DisposableObserver<Result>, params: Params)
}