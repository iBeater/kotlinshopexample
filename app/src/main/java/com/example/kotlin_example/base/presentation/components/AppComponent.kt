package com.example.kotlin_example.base.presentation.components

import android.app.Application
import android.content.res.Resources
import com.example.kotlin_example.base.domain.executors.PostExecutionThread
import com.example.kotlin_example.base.presentation.modules.AppModule
import com.example.kotlin_example.base.presentation.managers.AppResources
import com.example.kotlin_example.base.presentation.modules.NetModule
import com.example.kotlin_example.base.domain.executors.ThreadExecutor
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface AppComponent {

    fun applicationContext(): Application

    fun retrofit(): Retrofit

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun resources(): Resources

    fun appResources(): AppResources

}