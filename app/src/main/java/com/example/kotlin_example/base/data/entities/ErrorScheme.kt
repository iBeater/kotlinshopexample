package com.example.kotlin_example.base.data.entities

import com.example.kotlin_example.base.presentation.dialogs.ErrorDialog

data class ErrorScheme(
    val title: String = "",
    val description: String = "",
    val btnListener: ((dialog: ErrorDialog) -> Unit)? = null,
    val btnMessage: String = ""
)