package com.example.kotlin_example.base.presentation.states

sealed class BaseViewModelStates<out T> {

    class Data<T>(val data: T) : BaseViewModelStates<T>()
    class ProgressVisibility<T>(val visibility: Int) : BaseViewModelStates<T>()
    class Error<T>(val error: String) : BaseViewModelStates<T>()

}