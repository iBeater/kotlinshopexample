package com.example.kotlin_example.base.presentation.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseActivity : AppCompatActivity() {

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun getLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        this.setContentView(getLayout())

        this.initView(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun replaceFragment(container: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = this.supportFragmentManager
            .beginTransaction()
            .replace(container, fragment)
        if (addToBackStack) transaction.addToBackStack(fragment::class.java.name)
        transaction.commit()
    }

    fun addFragment(container: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = this.supportFragmentManager
            .beginTransaction()
            .add(container, fragment)
        if (addToBackStack) transaction.addToBackStack(fragment::class.java.name)
        transaction.commit()
    }

}