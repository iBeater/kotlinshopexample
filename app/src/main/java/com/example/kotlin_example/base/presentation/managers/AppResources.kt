package com.example.kotlin_example.base.presentation.managers

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import javax.inject.Inject

class AppResources @Inject constructor(private val context: Context) {

    fun getString(@StringRes resId: Int): String =
        this.context.resources.getString(resId)

    fun getStringArray(resId: Int): List<String> =
        this.context.resources.getStringArray(resId).toList()

    fun getDrawable(resId: Int): Drawable? =
        ContextCompat.getDrawable(context, resId)
}