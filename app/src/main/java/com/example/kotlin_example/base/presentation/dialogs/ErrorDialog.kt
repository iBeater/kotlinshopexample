package com.example.kotlin_example.base.presentation.dialogs

import android.os.Bundle
import android.view.View
import com.example.kotlin_example.R
import com.example.kotlin_example.base.data.entities.ErrorScheme

class ErrorDialog : BaseBottomSheetDialog(){

    private var errorSchema: ErrorScheme = ErrorScheme()

    override fun getStyle(): Int = R.style.BottomSheetDialogTheme

    override fun getLayout(): Int = R.layout.content_error

    override fun initView(view: View, saveInstanceState: Bundle?) {

    }

    object Builder{

    }
}