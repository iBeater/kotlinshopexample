package com.example.kotlin_example.base.presentation.modules

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.example.kotlin_example.base.presentation.managers.AppResources
import com.example.kotlin_example.base.domain.executors.JobExecutor
import com.example.kotlin_example.base.domain.executors.PostExecutionThread
import com.example.kotlin_example.base.domain.executors.ThreadExecutor
import com.example.kotlin_example.base.domain.executors.UiThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplication(): Application = this.application

    @Provides
    @Singleton
    fun providesContext(): Context = this.application.applicationContext

    @Provides
    @Singleton
    fun providesThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providesPostExecutionThread(uiThread: UiThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun providesResource(): Resources = application.resources

    @Provides
    @Singleton
    fun providesAppResource(context: Context): AppResources =
        AppResources(
            context
        )

}