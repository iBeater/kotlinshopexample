package com.example.kotlin_example.base.presentation.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.telephony.TelephonyManager
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.dialogs.ErrorDialog
import com.example.kotlin_example.base.presentation.lifecycle.SmartLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.io.Serializable
import java.util.concurrent.TimeUnit

fun <T> SmartLiveData<T>.observeWithValidations(
    viewLifecycleOwner: LifecycleOwner,
    output: (response: T) -> Unit
) {
    if (!this.hasObservers()) {
        this.observe(viewLifecycleOwner, Observer(output))
    }
}

fun <T> LiveData<T>.observeWithValidations(
    viewLifecycleOwner: LifecycleOwner,
    output: (response: T) -> Unit
) {
    if (!this.hasObservers()) {
        this.observe(viewLifecycleOwner, Observer(output))
    }
}


@Suppress("UNCHECKED_CAST")
fun <F : Fragment> AppCompatActivity.findFragmentByClassName(fragmentClass: Class<F>): F? {
    val navHostFragment = supportFragmentManager.primaryNavigationFragment as? NavHostFragment
    val desiredFragment = navHostFragment?.childFragmentManager?.primaryNavigationFragment as? F
    if (desiredFragment != null) {
        return desiredFragment
    } else {
        navHostFragment?.childFragmentManager?.fragments?.forEach {
            if (fragmentClass.isAssignableFrom(it.javaClass)) {
                return it as? F
            }
        }
    }
    return null
}

@Suppress("UNCHECKED_CAST")
fun <F : Fragment> Fragment.findFragmentByClassName(fragmentClass: Class<F>): F? {
    val navHostFragment = childFragmentManager.primaryNavigationFragment as? NavHostFragment
    val desiredFragment = navHostFragment?.childFragmentManager?.primaryNavigationFragment as? F
    if (desiredFragment != null) {
        return desiredFragment
    } else {
        navHostFragment?.childFragmentManager?.fragments?.forEach {
            if (fragmentClass.isAssignableFrom(it.javaClass)) {
                return it as? F
            }
        }
    }
    return null
}

fun View.show(show: Boolean) {
    val visibility = if (show) View.VISIBLE else View.GONE
    this.visibility = visibility
}

fun View.isVisible(): Boolean = this.visibility == View.VISIBLE

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

inline fun <reified T : AppCompatActivity> Activity.intentFor(
    extraName: String? = null,
    extras: Any? = null
): Intent = Intent(this, T::class.java).apply {
    extras?.let {
        when (it) {
            is String -> putExtra(extraName, it)
            is Int -> putExtra(extraName, it)
            is Float -> putExtra(extraName, it)
            is Double -> putExtra(extraName, it)
            is Boolean -> putExtra(extraName, it)
            is Serializable -> putExtra(extraName, it)
            else -> throw IllegalArgumentException("Wrong extra for argument $it")
        }
    }
}

inline fun <reified T : AppCompatActivity> Fragment.intentFor(
    extraName: String? = null,
    extras: Any? = null
): Intent = Intent(requireActivity(), T::class.java).apply {
    extras?.let {
        when (it) {
            is String -> putExtra(extraName, it)
            is Int -> putExtra(extraName, it)
            is Float -> putExtra(extraName, it)
            is Double -> putExtra(extraName, it)
            is Boolean -> putExtra(extraName, it)
            is Serializable -> putExtra(extraName, it)
            else -> throw IllegalArgumentException("Wrong extra for argument $it")
        }
    }
}

inline fun <reified T : Service> Fragment.intentForService(
    extraName: String? = null,
    extras: Any? = null
): Intent = Intent(requireActivity(), T::class.java).apply {
    extras?.let {
        when (it) {
            is String -> putExtra(extraName, it)
            is Int -> putExtra(extraName, it)
            is Float -> putExtra(extraName, it)
            is Double -> putExtra(extraName, it)
            is Boolean -> putExtra(extraName, it)
            is Serializable -> putExtra(extraName, it)
            else -> throw IllegalArgumentException("Wrong extra for argument $it")
        }
    }
}

fun EditText.trimmedValue(): String =
    this.text?.toString()?.trim() ?: ""

inline fun <reified T : Any> String.toSafeValue(defaultValue: T): T = try {
    when (T::class) {
        Int::class -> this.toInt() as T
        Float::class -> this.toFloat() as T
        Long::class -> this.toLong() as T
        else -> throw UnsupportedOperationException("Value type not supported. You need to specify default value with proper type.")
    }
} catch (e: Exception) {
    when (T::class) {
        Int::class -> defaultValue
        Float::class -> defaultValue
        Long::class -> defaultValue
        else -> throw UnsupportedOperationException("Value type not supported. You need to specify default value with proper type.")
    }
}

@SuppressLint("MissingPermission")
fun Activity.getPhoneNumber(): String {
    val telephonyManager = (getSystemService(Context.TELEPHONY_SERVICE)) as? TelephonyManager?
    return telephonyManager?.line1Number ?: ""
}

fun String.skip(number: Int): String =
    if (number >= this.length) "" else this.substring(number)

fun <T> Collection<T>.lastIndex(): Int = if (this.isEmpty()) -1 else this.size - 1

@Suppress("DEPRECATION") // Deprecated for third party Services.
fun <T> Context.isServiceRunning(service: Class<T>) =
    (getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
        .getRunningServices(Integer.MAX_VALUE)
        .any { it.service.className == service.name }


fun Fragment.alertDialog(
    title: String = "",
    message: String = "",
    okMessage: String = "Ok",
    okBtn: ((dialog: DialogInterface) -> Unit)? = null,
    noMessage: String = "No",
    noBtn: ((dialog: DialogInterface) -> Unit)? = null,
    isCancelable: Boolean = true
) {
    this.requireContext()
        .buildDialog(title, message, okMessage, okBtn, noMessage, noBtn, isCancelable)
}

fun Activity.alertDialog(
    title: String = "",
    message: String = "",
    okMessage: String = "Ok",
    okBtn: ((dialog: DialogInterface) -> Unit)? = null,
    noMessage: String = "No",
    noBtn: ((dialog: DialogInterface) -> Unit)? = null,
    isCancelable: Boolean = true
) {
    this.buildDialog(title, message, okMessage, okBtn, noMessage, noBtn, isCancelable)
}

private fun Context.buildDialog(
    title: String = "",
    message: String = "",
    okMessage: String = "Ok",
    okBtn: ((dialog: DialogInterface) -> Unit)? = null,
    noMessage: String = "No",
    noBtn: ((dialog: DialogInterface) -> Unit)? = null,
    isCancelable: Boolean = true
) {
    val builder = AlertDialog.Builder(this)
    if (title.isNotEmpty()) builder.setTitle(title)
    if (message.isNotEmpty()) builder.setMessage(message)
    okBtn?.let { builder.setPositiveButton(okMessage) { dialog, _ -> it(dialog) } }
    noBtn?.let { builder.setNegativeButton(noMessage) { dialog, _ -> it(dialog) } }
    builder.setCancelable(isCancelable)
    builder.show()
}


fun String.substringBetween(firstDelimiter: String, secondDelimiter: String): String {
    val firstIndex = this.indexOf(firstDelimiter) + 1
    val secondIndex = this.indexOf(secondDelimiter)
    return if (secondIndex > firstIndex) {
        this.substring(firstIndex, secondIndex)
    } else {
        ""
    }
}

fun wait(millis: Long, callback: () -> Unit) =
    Completable.timer(millis, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { callback() }

fun Fragment.errorDialog(
    title: String? = null,
    description: String,
    btn: ((dialog: ErrorDialog) -> Unit)? = null,
    btnMessage: String? = null
) {
    showErrorDialog(
        title,
        description,
        btn,
        btnMessage,
        childFragmentManager,
        this::class.java.name,
        resources
    )
}

fun AppCompatActivity.errorDialog(
    title: String? = null,
    description: String,
    btn: ((dialog: ErrorDialog) -> Unit)? = null,
    btnMessage: String? = null
) {
    showErrorDialog(
        title,
        description,
        btn,
        btnMessage,
        supportFragmentManager,
        this::class.java.name,
        resources
    )
}

fun BottomSheetDialogFragment.errorDialog(
    title: String? = null,
    description: String,
    btn: ((dialog: ErrorDialog) -> Unit)? = null,
    btnMessage: String? = null
) {
    showErrorDialog(
        title,
        description,
        btn,
        btnMessage,
        childFragmentManager,
        this::class.java.name,
        resources
    )
}

private fun showErrorDialog(
    title: String? = null,
    description: String,
    btn: ((dialog: ErrorDialog) -> Unit)? = null,
    btnMessage: String? = null,
    fragmentManager: FragmentManager,
    tag: String,
    resources: Resources
) {
    /*  ErrorDialog.Builder
          .setTitle(title ?: resources.getString(R.string.error_title))
          .setDescription(description)
          .setButtonListener(btn)
          .setButtonMessage(btnMessage ?: resources.getString(R.string.accept))
          .build()
          .show(fragmentManager, tag)*/
}

