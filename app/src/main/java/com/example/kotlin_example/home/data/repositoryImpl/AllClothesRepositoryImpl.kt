package com.example.kotlin_example.home.data.repositoryImpl

import com.example.kotlin_example.home.data.entities.AllGenericResponse
import com.example.kotlin_example.home.domain.dataSourceAbstractions.AllClothesDataSource
import com.example.kotlin_example.home.domain.repositoryAbstractions.AllClothesRepository
import io.reactivex.Observable
import javax.inject.Inject

class AllClothesRepositoryImpl
@Inject constructor(
    private val allClothesDataSource: AllClothesDataSource
) : AllClothesRepository {

    override fun getAllClothes(): Observable<List<AllGenericResponse>> =
        this.allClothesDataSource.getAllClothes()

}