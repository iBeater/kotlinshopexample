package com.example.kotlin_example.home.domain.useCases

import com.example.kotlin_example.base.domain.executors.PostExecutionThread
import com.example.kotlin_example.base.domain.executors.ThreadExecutor
import com.example.kotlin_example.base.domain.useCases.UseCase
import com.example.kotlin_example.home.data.entities.AllGenericResponse
import com.example.kotlin_example.home.domain.dataSourceAbstractions.AllClothesDataSource
import io.reactivex.Observable
import javax.inject.Inject

class AllClothesUseCase
@Inject constructor(
    private val allClothesDataSource: AllClothesDataSource,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<List<AllGenericResponse>, Unit>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Unit): Observable<List<AllGenericResponse>> =
        this.allClothesDataSource.getAllClothes()

}