package com.example.kotlin_example.home.domain.dataSourceAbstractions

import com.example.kotlin_example.home.data.entities.AllGenericResponse
import io.reactivex.Observable

interface AllClothesDataSource {
    fun getAllClothes(): Observable<List<AllGenericResponse>>
}