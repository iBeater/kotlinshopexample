package com.example.kotlin_example.home.presentation.viewModels

import android.app.Application
import android.content.res.Resources
import androidx.lifecycle.AndroidViewModel
import com.example.kotlin_example.base.presentation.lifecycle.BaseLiveData
import com.example.kotlin_example.base.presentation.lifecycle.SmartLiveData
import com.example.kotlin_example.home.domain.useCases.AllClothesUseCase
import com.example.kotlin_example.home.presentation.observers.AllClothesObserver
import com.example.kotlin_example.home.presentation.states.AllClothesState
import javax.inject.Inject

class AllClothesViewModel
@Inject constructor(
    private val allClothesUseCase: AllClothesUseCase,
    private val resources: Resources,
    app: Application
) : AndroidViewModel(app) {

    private val allClothesLiveData: SmartLiveData<AllClothesState> = SmartLiveData()

    private val timeLiveData: SmartLiveData<BaseLiveData<String>> = SmartLiveData()

    fun allClothes(): SmartLiveData<AllClothesState> =
        with(this.allClothesLiveData){
            allClothesUseCase.execute(AllClothesObserver(resources, this), Unit)
            this
        }

    override fun onCleared() {
        super.onCleared()
        this.allClothesUseCase.dispose()
    }
}