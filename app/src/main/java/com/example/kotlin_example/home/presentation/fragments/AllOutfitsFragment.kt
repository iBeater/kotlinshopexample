package com.example.kotlin_example.home.presentation.fragments;

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_example.Outfits.OutfitsActivity
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.fragments.BaseFragment
import com.example.kotlin_example.home.data.entities.AllGenericResponse
import com.example.kotlin_example.home.presentation.activities.HomeActivity
import com.example.kotlin_example.home.presentation.adapters.OutfitsAdapter
import kotlinx.android.synthetic.main.fragment_all_outfits.*

class AllOutfitsFragment : BaseFragment(), OutfitsAdapter.AllOutfitsCallback {

    private val outfitsAdapter by lazy { OutfitsAdapter() }

    override fun getLayout(): Int = R.layout.fragment_all_outfits

    override fun initView(view: View, saveInstanceState: Bundle?) {
        val allGenericResponseList: MutableList<AllGenericResponse> = ArrayList()

        val allGenericResponse = AllGenericResponse(
            1,
            "vintage",
            "Vintage"
        )
        allGenericResponseList.add(allGenericResponse)

        val allGenericResponse2 = AllGenericResponse(
            2,
            "casual",
            "Casual"
        )
        allGenericResponseList.add(allGenericResponse2)

        val allGenericResponse3 = AllGenericResponse(
            3,
            "girly",
            "Girly"
        )
        allGenericResponseList.add(allGenericResponse3)

        val allGenericResponse4 = AllGenericResponse(
            4,
            "grunge",
            "Grunge"
        )
        allGenericResponseList.add(allGenericResponse4)

        val allGenericResponse5 = AllGenericResponse(
            5,
            "sporty",
            "Sporty"
        )
        allGenericResponseList.add(allGenericResponse5)

        outfitsAdapter.setOutfits(allGenericResponseList)
        outfitsAdapter.setAllOutfitsCallback(this)
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        this.recycler_outfits.setHasFixedSize(false)
        this.recycler_outfits.layoutManager =
            GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false)
        this.recycler_outfits.adapter = outfitsAdapter
    }

    override fun outfitSelected(idType: Int) {
        val i = Intent(context, OutfitsActivity::class.java)
        startActivity(i)

    }

}
