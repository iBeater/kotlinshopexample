package com.example.kotlin_example.home.data.dataSourceImpl

import com.example.kotlin_example.base.data.handler.ApiResponseHandler
import com.example.kotlin_example.home.data.services.AllClothesService
import com.example.kotlin_example.home.domain.dataSourceAbstractions.AllClothesDataSource
import com.example.kotlin_example.home.data.entities.AllGenericResponse
import io.reactivex.Observable
import javax.inject.Inject

class AllClothesDataSourceImpl
@Inject constructor(
    private val apiResponseHandler: ApiResponseHandler,
    private val allClothesService: AllClothesService
) : AllClothesDataSource {

    override fun getAllClothes(): Observable<List<AllGenericResponse>> =
        this.allClothesService.getAllClothes()
            .flatMap { this.apiResponseHandler.handle(it) }

}