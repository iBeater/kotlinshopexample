package com.example.kotlin_example.home.presentation.callbacks

import com.example.kotlin_example.home.data.entities.AllGenericResponse

interface ClickClothesItemAdapterCallback {
    fun onItemSelected(allGenericResponse: AllGenericResponse)
}