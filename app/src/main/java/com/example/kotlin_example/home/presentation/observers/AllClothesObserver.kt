package com.example.kotlin_example.home.presentation.observers

import android.content.res.Resources
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.lifecycle.SmartLiveData
import com.example.kotlin_example.base.presentation.observer.BaseObserver
import com.example.kotlin_example.home.data.entities.AllGenericResponse
import com.example.kotlin_example.home.presentation.states.AllClothesState

class AllClothesObserver(
    resources: Resources,
    allClothesState: SmartLiveData<AllClothesState>
) : BaseObserver<List<AllGenericResponse>, AllClothesState>(resources, allClothesState) {

    override fun onProgressVisibility(visibility: Int): AllClothesState =
        AllClothesState.ProgressVisibility(visibility)

    override fun onHandleData(data: List<AllGenericResponse>): AllClothesState =
        AllClothesState.Data(data)

    override fun onHandleError(error: Throwable): AllClothesState =
        AllClothesState.Error(getString(R.string.generic_error))

}