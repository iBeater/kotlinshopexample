package com.example.kotlin_example.home.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_example.R
import com.example.kotlin_example.home.data.entities.AllGenericResponse

class OutfitsAdapter : RecyclerView.Adapter<OutfitsAdapter.ViewHolder>() {

    var allGenericOutfitsResponse: List<AllGenericResponse> = ArrayList()
    private var allOutfitsCallback: AllOutfitsCallback? = null

    fun setOutfits(allGenericOutfitsResponse: List<AllGenericResponse>) {
        this.allGenericOutfitsResponse = allGenericOutfitsResponse
    }

    fun setAllOutfitsCallback(allOutfitsCallback: AllOutfitsCallback) {
        this.allOutfitsCallback = allOutfitsCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            layoutInflater.inflate(R.layout.item_all_outfits, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = allGenericOutfitsResponse.get(position)
        holder.nameOutfits.text = item.nameType

        customImage(item.descriptionType, holder.imageOutfits)
        holder.imageOutfits.setOnClickListener { allOutfitsCallback?.outfitSelected(item.idType) }
    }

    override fun getItemCount(): Int {
        return allGenericOutfitsResponse.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageOutfits = itemView.findViewById(R.id.image_outfits) as ImageView
        val nameOutfits = itemView.findViewById(R.id.tv_name_outfits) as TextView
    }

    private fun customImage(descriptionType: String, imageView: ImageView) {
        when (descriptionType) {
            "vintage" -> imageView.setImageResource(R.drawable.vintage)
            "casual" -> imageView.setImageResource(R.drawable.casual)
            "girly" -> imageView.setImageResource(R.drawable.girly)
            "grunge" -> imageView.setImageResource(R.drawable.grunge)
            "sporty" -> imageView.setImageResource(R.drawable.sporty)
        }
    }

    interface AllOutfitsCallback {
        fun outfitSelected(idType: Int)
    }

}