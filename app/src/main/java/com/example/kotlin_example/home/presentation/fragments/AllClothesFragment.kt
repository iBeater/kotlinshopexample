package com.example.kotlin_example.home.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.extensions.observeWithValidations
import com.example.kotlin_example.base.presentation.fragments.BaseFragment
import com.example.kotlin_example.getAppComponent
import com.example.kotlin_example.home.presentation.callbacks.ClickClothesItemAdapterCallback
import com.example.kotlin_example.home.presentation.adapters.ClothesAdapter
import com.example.kotlin_example.home.data.entities.AllGenericResponse
import com.example.kotlin_example.home.presentation.components.AllClothesComponent
import com.example.kotlin_example.home.presentation.components.DaggerAllClothesComponent
import com.example.kotlin_example.home.presentation.modules.AllClothesModule
import com.example.kotlin_example.home.presentation.states.AllClothesState
import com.example.kotlin_example.home.presentation.viewModels.AllClothesViewModel
import com.example.kotlin_example.home.presentation.viewModelsFactory.AllClothesViewModelFactory
import kotlinx.android.synthetic.main.fragment_all_clothes.*
import java.lang.IllegalArgumentException
import javax.inject.Inject

class AllClothesFragment : BaseFragment(),
    ClickClothesItemAdapterCallback {

    @Inject
    lateinit var allClothesViewModelFactory: AllClothesViewModelFactory

    private val clothesAdapter by lazy { ClothesAdapter() }

    private val allClothesViewModel by lazy {
        ViewModelProvider(this, this.allClothesViewModelFactory)
            .get(AllClothesViewModel::class.java)
    }

    private val allClothesComponent: AllClothesComponent by lazy {
        DaggerAllClothesComponent.builder()
            .appComponent(requireActivity().getAppComponent())
            .allClothesModule(AllClothesModule())
            .build()
    }

    override fun getLayout(): Int = R.layout.fragment_all_clothes

    override fun initView(view: View, saveInstanceState: Bundle?) {
        this.allClothesComponent.inject(this)

        getAllClothes()
    }

    private fun getAllClothes() {
        this.allClothesViewModel.allClothes()
            .observeWithValidations(this) {
                when (it) {
                    is AllClothesState.Data -> handleVerified(it.allGenericResponse)
                    is AllClothesState.Error -> showError(it.error)
                }
            }
    }

    private fun handleVerified(allGenericResponse: List<AllGenericResponse>) {
        clothesAdapter.setClothes(allGenericResponse)
        clothesAdapter.setListener(this)
        setUpRecyclerView()
    }

    private fun showError(error: String) {
        throw IllegalArgumentException(error)
    }

    private fun setUpRecyclerView() {
        this.recycler_clothes.setHasFixedSize(false)
        this.recycler_clothes.layoutManager =
            GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false)
        this.recycler_clothes.adapter = clothesAdapter
    }

    override fun onItemSelected(allGenericResponse: AllGenericResponse) {

    }

}