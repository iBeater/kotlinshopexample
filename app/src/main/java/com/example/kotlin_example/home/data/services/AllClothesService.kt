package com.example.kotlin_example.home.data.services

import com.example.kotlin_example.home.data.entities.AllGenericResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface AllClothesService {

    @GET("clothes")
    fun getAllClothes(): Observable<Response<List<AllGenericResponse>>>

}