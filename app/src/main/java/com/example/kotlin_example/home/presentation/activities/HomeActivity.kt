package com.example.kotlin_example.home.presentation.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.kotlin_example.Outfits.OutfitsActivity
import com.example.kotlin_example.R
import com.example.kotlin_example.base.presentation.activities.BaseActivity
import com.example.kotlin_example.home.presentation.fragments.AllClothesFragment
import com.example.kotlin_example.home.presentation.fragments.AllOutfitsFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {

    private val allClothesFragment by lazy { AllClothesFragment() }

    private val allOutfitsFragment by lazy { AllOutfitsFragment() }

    override fun initView(savedInstanceState: Bundle?) {
        setupTabs()
        this.bottom_navigation_home.selectedItemId = R.id.menu_all_items
    }

    override fun getLayout(): Int = R.layout.activity_home

    private fun setupTabs() {
        this.bottom_navigation_home.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_all_items -> {
                    replaceFragment(R.id.home_container, this.allClothesFragment, false)
                    true
                }
                R.id.menu_outfit -> {
                    replaceFragment(R.id.home_container, allOutfitsFragment, false)
                    true
                }
                else -> false
            }
        }
    }

   /* fun nextActivity(className: Class){
        val i = Intent(this, className)
        startActivity(i)
    }*/
}