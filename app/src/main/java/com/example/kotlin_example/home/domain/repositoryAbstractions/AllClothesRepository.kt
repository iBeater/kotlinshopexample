package com.example.kotlin_example.home.domain.repositoryAbstractions

import com.example.kotlin_example.home.data.entities.AllGenericResponse
import io.reactivex.Observable

interface AllClothesRepository{
    fun getAllClothes(): Observable<List<AllGenericResponse>>
}