package com.example.kotlin_example.home.presentation.states

import com.example.kotlin_example.home.data.entities.AllGenericResponse

sealed class AllClothesState {
    class Data(val allGenericResponse: List<AllGenericResponse>) : AllClothesState()
    class ProgressVisibility(val visibility: Int) : AllClothesState()
    class Error(val error: String) : AllClothesState()
}