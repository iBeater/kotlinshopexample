package com.example.kotlin_example.home.presentation.components

import com.example.kotlin_example.base.presentation.components.AppComponent
import com.example.kotlin_example.base.presentation.scopes.AppScope
import com.example.kotlin_example.home.presentation.fragments.AllClothesFragment
import com.example.kotlin_example.home.presentation.modules.AllClothesModule
import dagger.Component

@AppScope
@Component(dependencies = [AppComponent::class], modules = [AllClothesModule::class])
interface AllClothesComponent {
    fun inject(allClothesFragment: AllClothesFragment)
}