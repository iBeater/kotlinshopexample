package com.example.kotlin_example.home.data.entities

import com.google.gson.annotations.SerializedName

data class AllGenericResponse(
    @SerializedName("id_type") private val _idType: Int? = null,
    @SerializedName("name_clothes") private val _descriptionType: String? = null,
    @SerializedName("type_clothes") private val _nameType: String? = null
) {

    val idType: Int
        get() = this._idType ?: 0

    val descriptionType: String
        get() = this._descriptionType ?: ""

    val nameType: String
        get() = this._nameType ?: ""

}