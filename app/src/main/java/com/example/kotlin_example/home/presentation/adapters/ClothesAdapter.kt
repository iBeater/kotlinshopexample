package com.example.kotlin_example.home.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_example.R
import com.example.kotlin_example.home.presentation.callbacks.ClickClothesItemAdapterCallback
import com.example.kotlin_example.home.data.entities.AllGenericResponse

class ClothesAdapter : RecyclerView.Adapter<ClothesAdapter.ViewHolder>() {

    var allGenericResponse: List<AllGenericResponse> = ArrayList()

    private var mListener: ClickClothesItemAdapterCallback? = null

    fun setClothes(allGenericResponse: List<AllGenericResponse>) {
        this.allGenericResponse = allGenericResponse
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            layoutInflater.inflate(R.layout.item_all_clothes, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = allGenericResponse.get(position)
        holder.nameClothes.text = item.descriptionType

        customIcons(item, holder.imageClothes)

        holder.imageClothes.setOnClickListener{
            mListener?.onItemSelected(item)
        }
    }

    override fun getItemCount(): Int {
        return allGenericResponse.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameClothes = itemView.findViewById(R.id.text_name) as TextView
        val imageClothes = itemView.findViewById(R.id.image_clothes) as ImageView
    }

    fun setListener(listener: ClickClothesItemAdapterCallback){
        this.mListener = listener
    }

    private fun customIcons(item: AllGenericResponse, imageView: ImageView) {
        when (item.nameType) {
            "dress" -> imageView.setImageResource(R.drawable.ic_dress)
            "glass" -> imageView.setImageResource(R.drawable.ic_glasses)
            "skirt" -> imageView.setImageResource(R.drawable.ic_skirt)
            "shoe" -> imageView.setImageResource(R.drawable.ic_shoes)
            "fit" -> imageView.setImageResource(R.drawable.ic_fitness)
        }
    }

}
