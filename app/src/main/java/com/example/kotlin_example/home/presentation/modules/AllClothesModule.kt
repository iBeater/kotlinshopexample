package com.example.kotlin_example.home.presentation.modules

import com.example.kotlin_example.base.presentation.scopes.AppScope
import com.example.kotlin_example.home.data.dataSourceImpl.AllClothesDataSourceImpl
import com.example.kotlin_example.home.data.repositoryImpl.AllClothesRepositoryImpl
import com.example.kotlin_example.home.data.services.AllClothesService
import com.example.kotlin_example.home.domain.dataSourceAbstractions.AllClothesDataSource
import com.example.kotlin_example.home.domain.repositoryAbstractions.AllClothesRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class AllClothesModule {

    @Provides
    @AppScope
    fun providesAllClothesService(retrofit: Retrofit): AllClothesService =
        retrofit.create(AllClothesService::class.java)

    @Provides
    @AppScope
    fun providesAllClothesApiDataSource(allClothesDataSourceImpl: AllClothesDataSourceImpl): AllClothesDataSource =
        allClothesDataSourceImpl

    @Provides
    @AppScope
    fun providesAllClothesRepository(allClothesApiDataSourceImpl: AllClothesRepositoryImpl): AllClothesRepository =
        allClothesApiDataSourceImpl

}