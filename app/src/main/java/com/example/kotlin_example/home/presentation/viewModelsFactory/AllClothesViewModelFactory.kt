package com.example.kotlin_example.home.presentation.viewModelsFactory

import android.app.Application
import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kotlin_example.home.domain.useCases.AllClothesUseCase
import javax.inject.Inject

class AllClothesViewModelFactory
@Inject constructor(
    private val allClothesUseCase: AllClothesUseCase,
    private val resources: Resources,
    private val app: Application
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = modelClass
        .getConstructor(
            AllClothesUseCase::class.java,
            Resources::class.java,
            Application::class.java
        )
        .newInstance(allClothesUseCase, resources, app)

}